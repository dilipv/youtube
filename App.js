import React from 'react';

import CameraPage from './camera.page';

export default class App extends React.Component {
    render() {
        return (
            <CameraPage />
        );
    };
};