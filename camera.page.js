/* eslint-disable no-console */
import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
// eslint-disable-next-line import/no-unresolved
import AntDesign from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {RNCamera} from 'react-native-camera';
import Gallery from './gallery.component';

const landmarkSize = 2;

export default class CameraScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      flash: false,
      type: 'back',
      captures: [],
      selectedImages: [],
    };
  }

  toggleFacing() {
    this.setState({
      type: this.state.type === 'back' ? 'front' : 'back',
    });
  }

  toggleFlash = () => {
    this.setState({
      flash: !this.state.flash,
    });
  };

  takePicture = async function () {
    let temp = this.state.captures;
    if (this.camera) {
      const data = await this.camera.takePictureAsync();
      // console.warn('takePicture ', data);
      temp.push({uri: data.uri});
      this.setState({
        captures: temp,
      });
    }
  };

  handleSelectImage = (uri) => {
    const {selectedImages, captures} = this.state;
    let temp = [];
    let temp1 = [];
    if (captures.length > 0) {
      if (selectedImages.includes(uri)) {
        temp1 = JSON.parse(JSON.stringify(selectedImages));
        var index = temp1.indexOf(uri);
        if (index >= 0) {
          temp1.splice(index, 1);
        }
        this.setState(
          {
            selectedImages: temp1,
          },
          () => console.log('selectedImages if...', this.state.selectedImages),
        );
      } else {
        temp = JSON.parse(JSON.stringify(selectedImages));
        temp.push(uri);
        this.setState({
          selectedImages: temp,
        });
      }
    }
  };

  renderCamera() {
    const {selectedImages, captures, flash, type} = this.state;
    console.log('captures', captures);

    return (
      <RNCamera
        ref={(ref) => {
          this.camera = ref;
        }}
        style={{
          flex: 1,
          justifyContent: 'space-between',
        }}
        type={this.state.type}
        flashMode={
          this.state.flash
            ? RNCamera.Constants.FlashMode.torch
            : RNCamera.Constants.FlashMode.off
        }>
        <View
          style={{
            alignItems: 'flex-end',
            flexDirection: 'row',
            flex: 1,
            paddingBottom: 200,
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-around',
              flex: 1,
            }}>
            <TouchableOpacity
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'flex-end',
              }}
              onPress={this.toggleFlash}>
              <Text style={styles.flipText}>
                <MaterialCommunityIcons
                  size={40}
                  name={flash ? 'flashlight-off' : 'flashlight'}
                />{' '}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{flex: 1, alignItems: 'center'}}
              onPress={this.takePicture.bind(this)}>
              <Text style={styles.flipText}>
                <AntDesign size={40} name="camera" />{' '}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'flex-end',
              }}
              onPress={this.toggleFacing.bind(this)}>
              <Text style={styles.flipText}>
                <MaterialIcons
                  size={35}
                  name={type === 'front' ? 'camera-rear' : 'camera-front'}
                />{' '}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        {captures.length > 0 && (
          <Gallery
            captures={captures}
            selectedImages={selectedImages}
            handleSelectImage={this.handleSelectImage}
          />
        )}
      </RNCamera>
    );
  }

  render() {
    return <View style={styles.container}>{this.renderCamera()}</View>;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 10,
    backgroundColor: '#000',
  },
  flipButton: {
    // flex: 0.3,
    height: 40,
    marginHorizontal: 2,
    marginBottom: 10,
    marginTop: 10,
    borderRadius: 8,
    borderColor: 'white',
    borderWidth: 1,
    padding: 5,
    alignItems: 'center',
    justifyContent: 'center',
    width: 50,
  },
  autoFocusBox: {
    position: 'absolute',
    height: 64,
    width: 64,
    borderRadius: 12,
    borderWidth: 2,
    borderColor: 'white',
    opacity: 0.4,
  },
  flipText: {
    color: 'white',
    fontSize: 20,
  },
  zoomText: {
    position: 'absolute',
    bottom: 70,
    zIndex: 2,
    left: 2,
  },
  picButton: {
    backgroundColor: 'darkseagreen',
  },
  facesContainer: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    left: 0,
    top: 0,
  },
  face: {
    padding: 10,
    borderWidth: 2,
    borderRadius: 2,
    position: 'absolute',
    borderColor: '#FFD700',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  landmark: {
    width: landmarkSize,
    height: landmarkSize,
    position: 'absolute',
    backgroundColor: 'red',
  },
  faceText: {
    color: '#FFD700',
    fontWeight: 'bold',
    textAlign: 'center',
    margin: 10,
    backgroundColor: 'transparent',
  },
  text: {
    padding: 10,
    borderWidth: 2,
    borderRadius: 2,
    position: 'absolute',
    borderColor: '#F00',
    justifyContent: 'center',
  },
  textBlock: {
    color: '#F00',
    position: 'absolute',
    textAlign: 'center',
    backgroundColor: 'transparent',
  },
});
