import React from 'react';
import {View, Image, ScrollView, TouchableOpacity, Text} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';

import styles from './styles';

export default ({captures = [], handleSelectImage, selectedImages = []}) => {
  const checkInSelectedImages = (uri) => selectedImages.includes(uri);

  return (
    <ScrollView
      horizontal={true}
      style={[styles.bottomToolbar, styles.galleryContainer]}>
      {captures.map(({uri}, i) => (
        <View
          key={i}
          style={{
            width: 75,
            height: 75,
            marginRight: 5,
          }}>
          <TouchableOpacity
            activeOpacity={0.9}
            style={styles.galleryImageContainer}
            onPress={() => handleSelectImage(uri)}
            key={uri}>
            <Image source={{uri}} style={styles.galleryImage} />
          </TouchableOpacity>
          {checkInSelectedImages(uri) && (
            <View
              style={{
                position: 'absolute',
                width: '100%',
                height: '100%',
                backgroundColor: '#378ad352',
              }}>
              <TouchableOpacity
                activeOpacity={0.9}
                style={{
                  flex: 1,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}
                onPress={() => handleSelectImage(uri)}>
                <AntDesign name="dingding" color="goldenrod" size={45} />
              </TouchableOpacity>
              <View
                style={{
                  width: 15,
                  height: 15,
                  borderRadius: 15,
                  position: 'absolute',
                  alignItems: 'center',
                  justifyContent: 'center',
                  top: 1,
                  right: 1,
                  zIndex: 1,
                  backgroundColor: 'seagreen',
                  borderWidth: 1,
                  borderColor: 'black',
                }}>
                <Text style={{color: 'black'}}>{i + 1}</Text>
              </View>
            </View>
          )}
        </View>
      ))}
    </ScrollView>
  );
};
